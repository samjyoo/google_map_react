Directions:
1. run npm install
2. npm run-script dev - to compile the files and set up dev server on http://localhost:8080
3. You may need CORS tool from Chrome to handle api request to Google Maps. You can download from: https://chrome.google.com/webstore/detail/allow-control-allow-origi/nlfbmbojpeacfghkpbjhddihlkkiljbi?hl=en

Extra:
1. npm run-script test:update - to run test with Jest
2. npm run-script build - to build production code with webpack. Also with npm run-script build:dev and npm run-script watch.
